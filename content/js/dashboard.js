/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.11096014492753623, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "findAllChildActiveSubsidies"], "isController": false}, {"data": [0.4523809523809524, 500, 1500, "getUpcomingEvents"], "isController": false}, {"data": [0.0, 500, 1500, "findAllFeeGroup"], "isController": false}, {"data": [0.0, 500, 1500, "getListDomainByLevel"], "isController": false}, {"data": [0.0, 500, 1500, "getPendingEvents"], "isController": false}, {"data": [0.3229166666666667, 500, 1500, "getPortfolioTermByYear"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCurrentFeeTier"], "isController": false}, {"data": [0.4318181818181818, 500, 1500, "findAllClassResources"], "isController": false}, {"data": [0.0, 500, 1500, "listBulkInvoiceRequest"], "isController": false}, {"data": [0.019230769230769232, 500, 1500, "findAllCustomSubsidies"], "isController": false}, {"data": [0.0, 500, 1500, "findAllUploadedSubsidyFiles"], "isController": false}, {"data": [0.03125, 500, 1500, "getChildFinancialAssistanceStatus"], "isController": false}, {"data": [0.0, 500, 1500, "getAllChildDiscounts"], "isController": false}, {"data": [0.0625, 500, 1500, "getMyDownloadPortfolio"], "isController": false}, {"data": [0.15476190476190477, 500, 1500, "getListDomain"], "isController": false}, {"data": [0.038461538461538464, 500, 1500, "getAdvancePaymentReceipts"], "isController": false}, {"data": [0.5333333333333333, 500, 1500, "getLessonPlan"], "isController": false}, {"data": [0.016666666666666666, 500, 1500, "findAllProgramBillingUpload"], "isController": false}, {"data": [0.0, 500, 1500, "findAllChildHistorySubsidiesForBillingAdjustment"], "isController": false}, {"data": [0.03125, 500, 1500, "getChildSemesterEvaluation"], "isController": false}, {"data": [0.07692307692307693, 500, 1500, "getCentreManagementConfig"], "isController": false}, {"data": [0.275, 500, 1500, "updateClassResourceOrder"], "isController": false}, {"data": [0.0, 500, 1500, "getChildPortfolio"], "isController": false}, {"data": [0.7941176470588235, 500, 1500, "getStaffCheckInOutRecordsByRole"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCentreForSchool"], "isController": false}, {"data": [0.0, 500, 1500, "portfolioByID"], "isController": false}, {"data": [0.0, 500, 1500, "findAllAbsentForVoidingSubsidyByChild"], "isController": false}, {"data": [0.0, 500, 1500, "invoicesByFkChild"], "isController": false}, {"data": [0.45, 500, 1500, "getCountStaffCheckInOutRecordsByRole"], "isController": false}, {"data": [0.0, 500, 1500, "findAllFeeDraft"], "isController": false}, {"data": [0.02631578947368421, 500, 1500, "getPastEvents"], "isController": false}, {"data": [0.0, 500, 1500, "findAllConsolidatedRefund"], "isController": false}, {"data": [0.14583333333333334, 500, 1500, "getMyDownloadAlbum"], "isController": false}, {"data": [0.0, 500, 1500, "getRefundChildBalance"], "isController": false}, {"data": [0.0, 500, 1500, "findAllUploadedGiroFiles"], "isController": false}, {"data": [0.0, 500, 1500, "findAllInvoice"], "isController": false}, {"data": [0.9565217391304348, 500, 1500, "getAllArea"], "isController": false}, {"data": [0.0, 500, 1500, "getChildChecklist"], "isController": false}, {"data": [0.0, 500, 1500, "bankAccountInfoByIDChild"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCreditDebitNotes"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 1104, 0, 0.0, 47561.218297101484, 12, 369056, 24605.0, 118062.0, 170070.0, 359894.0, 2.5377443498409313, 132.00292413833927, 4.562465196525773], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["findAllChildActiveSubsidies", 18, 0, 0.0, 31466.499999999996, 2240, 94716, 21815.0, 89286.3, 94716.0, 94716.0, 0.05456314623226319, 0.06112599861924928, 0.0889315342398899], "isController": false}, {"data": ["getUpcomingEvents", 21, 0, 0.0, 4901.2380952380945, 16, 21050, 1083.0, 15770.400000000001, 20588.699999999993, 21050.0, 0.07513685641704533, 0.04380537429782819, 0.10786247942681312], "isController": false}, {"data": ["findAllFeeGroup", 26, 0, 0.0, 19252.57692307692, 3306, 51492, 13856.5, 39640.0, 47500.599999999984, 51492.0, 0.10080606077054602, 0.09253681359796218, 0.1085830908495237], "isController": false}, {"data": ["getListDomainByLevel", 25, 0, 0.0, 38239.36000000001, 2776, 207491, 22782.0, 90595.0, 172831.69999999992, 207491.0, 0.07118106469789331, 0.37608348702511557, 0.12658273321764038], "isController": false}, {"data": ["getPendingEvents", 26, 0, 0.0, 25999.423076923074, 3528, 94598, 18692.0, 66330.20000000001, 87787.34999999998, 94598.0, 0.09613891333446728, 0.047975571009680446, 0.12505569586085002], "isController": false}, {"data": ["getPortfolioTermByYear", 48, 0, 0.0, 13463.479166666664, 30, 107642, 2895.5, 44986.8, 53864.39999999998, 107642.0, 0.1420294829535031, 0.14187344470318797, 0.18024151669734106], "isController": false}, {"data": ["findAllCurrentFeeTier", 18, 0, 0.0, 102843.72222222223, 60319, 154393, 93428.0, 141179.2, 154393.0, 154393.0, 0.053255383231655, 0.4369039049139925, 0.13480268880512672], "isController": false}, {"data": ["findAllClassResources", 22, 0, 0.0, 2218.2272727272725, 71, 12541, 1412.0, 8048.599999999995, 12164.049999999996, 12541.0, 0.08394223248182842, 0.04926687668122937, 0.11656821737222657], "isController": false}, {"data": ["listBulkInvoiceRequest", 32, 0, 0.0, 43777.28125000001, 2006, 125685, 39188.0, 75550.09999999999, 118510.94999999998, 125685.0, 0.09199341097193914, 0.1404157239737704, 0.13933767618894297], "isController": false}, {"data": ["findAllCustomSubsidies", 26, 0, 0.0, 14880.615384615383, 1374, 51606, 9309.5, 41493.6, 48598.79999999999, 51606.0, 0.10323851273010276, 0.12688441306920156, 0.12904814091262845], "isController": false}, {"data": ["findAllUploadedSubsidyFiles", 28, 0, 0.0, 28035.749999999993, 5045, 100895, 22298.0, 58237.60000000002, 87173.1499999999, 100895.0, 0.08427008562442628, 7.062660240132124, 0.1213851331015906], "isController": false}, {"data": ["getChildFinancialAssistanceStatus", 16, 0, 0.0, 44442.87499999999, 542, 75836, 48807.0, 70629.40000000001, 75836.0, 75836.0, 0.045791739170253684, 0.03295753102390329, 0.06005693916567451], "isController": false}, {"data": ["getAllChildDiscounts", 25, 0, 0.0, 25830.879999999997, 1892, 113269, 13029.0, 68228.00000000003, 102280.29999999997, 113269.0, 0.07588197620948281, 0.04638878623743774, 0.12501259166542725], "isController": false}, {"data": ["getMyDownloadPortfolio", 24, 0, 0.0, 12735.833333333332, 341, 77442, 4613.0, 50829.5, 75523.5, 77442.0, 0.07464079119238663, 0.04388062138458668, 0.07660885892890464], "isController": false}, {"data": ["getListDomain", 42, 0, 0.0, 26820.976190476184, 39, 92731, 14474.5, 68317.90000000004, 73488.7, 92731.0, 0.12250255213650284, 0.6165431994676972, 0.20410825616158668], "isController": false}, {"data": ["getAdvancePaymentReceipts", 26, 0, 0.0, 25890.153846153844, 687, 67521, 24891.5, 54268.80000000001, 66483.25, 67521.0, 0.07880053463131959, 0.11850861653538296, 0.12566530571576648], "isController": false}, {"data": ["getLessonPlan", 30, 0, 0.0, 4943.433333333334, 20, 63965, 686.0, 15025.600000000017, 43837.74999999997, 63965.0, 0.09592816898706248, 0.06248446163512762, 0.15382231784839512], "isController": false}, {"data": ["findAllProgramBillingUpload", 30, 0, 0.0, 32196.066666666666, 748, 124010, 21615.0, 77729.5, 113871.29999999999, 124010.0, 0.08951643223307691, 0.12578923654418828, 0.13348788283194185], "isController": false}, {"data": ["findAllChildHistorySubsidiesForBillingAdjustment", 26, 0, 0.0, 44575.15384615385, 8184, 94131, 41720.5, 81687.3, 90817.19999999998, 94131.0, 0.07464357692020603, 0.1092094069926677, 0.13419807139658133], "isController": false}, {"data": ["getChildSemesterEvaluation", 32, 0, 0.0, 18671.0625, 902, 73171, 10471.0, 53431.79999999999, 63877.29999999997, 73171.0, 0.10133123915439081, 0.07807651141876401, 0.15536137253163435], "isController": false}, {"data": ["getCentreManagementConfig", 26, 0, 0.0, 16209.076923076922, 574, 60553, 7502.0, 49044.600000000006, 58127.84999999999, 60553.0, 0.07973943605817299, 0.09406761597487594, 0.12910936033637774], "isController": false}, {"data": ["updateClassResourceOrder", 20, 0, 0.0, 2433.2, 143, 8557, 2087.5, 6424.900000000001, 8451.599999999999, 8557.0, 0.07522671451085709, 0.044372007387263365, 0.07155353509138165], "isController": false}, {"data": ["getChildPortfolio", 27, 0, 0.0, 161320.88888888888, 88928, 220859, 160088.0, 205582.0, 220597.8, 220859.0, 0.07000731187479581, 0.14124015684230723, 0.16934385890026293], "isController": false}, {"data": ["getStaffCheckInOutRecordsByRole", 17, 0, 0.0, 1670.8823529411766, 38, 19426, 295.0, 6449.999999999989, 19426.0, 19426.0, 0.06241784709830443, 0.03736537135865295, 0.11392476194016699], "isController": false}, {"data": ["findAllCentreForSchool", 51, 0, 0.0, 97807.96078431372, 12496, 175819, 97182.0, 149434.6, 163977.8, 175819.0, 0.14501325023031517, 132.57290214608946, 0.35664362833814817], "isController": false}, {"data": ["portfolioByID", 22, 0, 0.0, 39476.63636363637, 5263, 118235, 30271.5, 85746.9, 113889.94999999994, 118235.0, 0.07174373139146967, 0.12058348035363138, 0.2521539934354486], "isController": false}, {"data": ["findAllAbsentForVoidingSubsidyByChild", 29, 0, 0.0, 31285.206896551725, 7427, 111002, 28120.0, 65356.0, 96462.5, 111002.0, 0.0861093707780427, 0.05390244791867712, 0.11461628161178926], "isController": false}, {"data": ["invoicesByFkChild", 25, 0, 0.0, 55755.24, 5457, 150624, 58803.0, 133015.20000000004, 148692.6, 150624.0, 0.07203199373033527, 0.21027433294772208, 0.20934298177878688], "isController": false}, {"data": ["getCountStaffCheckInOutRecordsByRole", 30, 0, 0.0, 4521.366666666667, 12, 28436, 1094.5, 22628.500000000015, 25734.399999999998, 28436.0, 0.1058660371166326, 0.06389180755671772, 0.11568759329444521], "isController": false}, {"data": ["findAllFeeDraft", 30, 0, 0.0, 42574.43333333333, 21134, 78534, 37633.5, 71551.70000000001, 76228.4, 78534.0, 0.08403996940945113, 0.04776490448857477, 0.13385663096368633], "isController": false}, {"data": ["getPastEvents", 19, 0, 0.0, 19042.315789473687, 1129, 58631, 15577.0, 40814.0, 58631.0, 58631.0, 0.05829651448208149, 0.028920536481345113, 0.07343994500184094], "isController": false}, {"data": ["findAllConsolidatedRefund", 31, 0, 0.0, 80983.22580645164, 56620, 127736, 74592.0, 121529.40000000001, 126448.4, 127736.0, 0.0815010923775446, 0.11864180565670163, 0.1407963207186293], "isController": false}, {"data": ["getMyDownloadAlbum", 24, 0, 0.0, 13486.166666666666, 139, 44120, 4956.5, 35703.5, 42288.5, 44120.0, 0.08900327457881055, 0.05823456442168268, 0.14054520995501626], "isController": false}, {"data": ["getRefundChildBalance", 25, 0, 0.0, 27225.0, 3885, 58759, 24388.0, 53647.000000000015, 58470.1, 58759.0, 0.07642852557145609, 0.07292057566729745, 0.11964348290141026], "isController": false}, {"data": ["findAllUploadedGiroFiles", 24, 0, 0.0, 25853.625, 3075, 76347, 23466.5, 63350.0, 73254.5, 76347.0, 0.06885807490037098, 19.709845028088356, 0.09851277317289402], "isController": false}, {"data": ["findAllInvoice", 65, 0, 0.0, 193254.0769230769, 4802, 369056, 230568.0, 364050.0, 366417.1, 369056.0, 0.14941532633456622, 0.3274386592617963, 0.48082956466695326], "isController": false}, {"data": ["getAllArea", 23, 0, 0.0, 211.39130434782606, 50, 2244, 66.0, 278.00000000000017, 1860.7999999999945, 2244.0, 0.08134451879412057, 0.06468671418542306, 0.07649880038939268], "isController": false}, {"data": ["getChildChecklist", 29, 0, 0.0, 76099.58620689657, 11401, 142065, 82138.0, 117889.0, 134072.5, 142065.0, 0.08254441743565805, 0.20217767383427357, 0.2718161871025771], "isController": false}, {"data": ["bankAccountInfoByIDChild", 24, 0, 0.0, 62259.375, 15889, 98039, 68044.5, 94560.5, 97797.5, 98039.0, 0.06886301424890537, 0.1039473974443214, 0.15850598103971675], "isController": false}, {"data": ["findAllCreditDebitNotes", 22, 0, 0.0, 139296.54545454544, 88862, 200189, 133405.0, 182815.4, 197811.19999999995, 200189.0, 0.05595230828704551, 0.11527993559762152, 0.1307010951392704], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 1104, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
